package rodischeele.contactcard.main;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import rodischeele.contactcard.fragments.AddressFragment;
import rodischeele.contactcard.fragments.BirthdayFragment;
import rodischeele.contactcard.fragments.EmailFragment;
import rodischeele.contactcard.fragments.NameFragment;
import rodischeele.contactcard.fragments.PasswordFragment;
import rodischeele.contactcard.fragments.TelephoneFragment;
import rodischeele.contactcard.R;
import rodischeele.contactcard.utils.AsyncTaskLoadImage;

public class ContactActivity extends FragmentActivity {

    Context context;
    private FragmentTabHost mTabHost;
    private ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_card);

        context = this;

        Bundle selectedPerson = getIntent().getExtras();

        imageView = (ImageView)findViewById(R.id.image_picture);
        showImage(imageView, selectedPerson);

        tabHostMethod(selectedPerson);
    }

    public void tabHostMethod(Bundle currentPerson){

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        Bundle nameBundle = new Bundle();
        nameBundle.putString("name",currentPerson.getString("selectedPersonFirst"));
        Bundle emailBundle = new Bundle();
        emailBundle.putString("email",currentPerson.getString("selectedPersonEmail"));
        Bundle birthdayBundle = new Bundle();
        birthdayBundle.putString("birthday",currentPerson.getString("selectedPersonBirthday"));
        Bundle addressBundle = new Bundle();
        addressBundle.putString("address",currentPerson.getString("selectedPersonAddress"));
        Bundle telephoneBundle = new Bundle();
        telephoneBundle.putString("phone",currentPerson.getString("selectedPersonPhone"));
        Bundle passwordBundle = new Bundle();
        passwordBundle.putString("password",currentPerson.getString("selectedPersonPassword"));

        mTabHost.addTab(
                mTabHost.newTabSpec("NameTab").setIndicator("", getResources().getDrawable(R.drawable.ic_person)),
                NameFragment.class, nameBundle);
        mTabHost.addTab(
                mTabHost.newTabSpec("EmailTab").setIndicator("", getResources().getDrawable(R.drawable.ic_email)),
                EmailFragment.class, emailBundle);
        mTabHost.addTab(
                mTabHost.newTabSpec("BirthdayTab").setIndicator("", getResources().getDrawable(R.drawable.ic_action_calendar_month)),
                BirthdayFragment.class, birthdayBundle);
        mTabHost.addTab(
                mTabHost.newTabSpec("AddressTab").setIndicator("", getResources().getDrawable(R.drawable.ic_action_home)),
                AddressFragment.class, addressBundle);
        mTabHost.addTab(
                mTabHost.newTabSpec("TelephoneTab").setIndicator("", getResources().getDrawable(R.drawable.ic_call)),
                TelephoneFragment.class, telephoneBundle);
        mTabHost.addTab(
                mTabHost.newTabSpec("PasswordTab").setIndicator("", getResources().getDrawable(R.drawable.ic_lock)),
                PasswordFragment.class, passwordBundle);
    }

    public void showImage(View v, Bundle currentPerson) {
        new AsyncTaskLoadImage(imageView).execute(currentPerson.getString("selectedPersonPicture"));
    }

}
