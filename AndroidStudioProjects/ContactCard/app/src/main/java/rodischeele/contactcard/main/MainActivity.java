package rodischeele.contactcard.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import rodischeele.contactcard.R;
import rodischeele.contactcard.adapters.PersonAdapter;
import rodischeele.contactcard.classes.Person;
import rodischeele.contactcard.main.ContactActivity;
import rodischeele.contactcard.utils.RandomUserTask;


/**
 * Created by Rodi on 25-5-2016.
 */
public class MainActivity extends AppCompatActivity implements RandomUserTask.OnRandomUserAvailable,
        View.OnClickListener {

    // TAG for Log.i(...)
    private static final String TAG = "MainActivity";

    private Button newListView = null;
    private ListView personsListView = null;
    private int listCount;

    //
    private ArrayList<Person> persons = new ArrayList<Person>();
    private PersonAdapter personAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listCount = 0;

        // Inflate UI and set listeners and adapters and ...
        personsListView = (ListView) findViewById(R.id.personslistView);
        personAdapter = new PersonAdapter(getApplicationContext(),
                getLayoutInflater(),
                persons);
        personsListView.setAdapter(personAdapter);

        newListView = (Button) findViewById(R.id.newListView);
        newListView.setOnClickListener(this);

        personsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Selected user: ", position + "");

                Person selected = persons.get(position);

                Intent contactCard = new Intent(getApplicationContext(), ContactActivity.class);
                contactCard.putExtra("selectedPersonFirst", selected.first);
                contactCard.putExtra("selectedPersonLast", selected.last);
                contactCard.putExtra("selectedPersonEmail", selected.email);
                contactCard.putExtra("selectedPersonPhone", selected.phone);
                contactCard.putExtra("selectedPersonPassword", selected.password);
                contactCard.putExtra("selectedPersonBirthday", selected.birthday);
                contactCard.putExtra("selectedPersonAddress", selected.address);
                contactCard.putExtra("selectedPersonPicture", selected.imageUrl);

                startActivity(contactCard);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (listCount >= 5){
            listCount = 0;
            persons.clear();
        }
        for(int i = 0; i < 5; i++) {
            listCount++;
            Log.i(TAG, "onClick(...)");
            // Connect to ... and pass self for callback
            RandomUserTask getRandomUser = new RandomUserTask(this);
            String[] urls = new String[]{"http://api.randomuser.me/"};
            getRandomUser.execute(urls);
        }
    }

    @Override
    public void onRandomUserAvailable(Person person) {
        // Opslaag in array of mss wel in db?
        persons.add(person);
        Log.i(TAG, "Person added (" + person.toString() + ")");
        personAdapter.notifyDataSetChanged();
    }

}
