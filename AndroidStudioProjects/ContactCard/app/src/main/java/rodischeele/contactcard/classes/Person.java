package rodischeele.contactcard.classes;

import java.io.Serializable;

/**
 * Created by Rodi on 8-6-2016.
 */
public class Person implements Serializable {
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String last) {
        this.email = last;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String last) {
        this.birthday = last;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String last) {
        this.password = last;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String last) {
        this.phone = last;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String last) {
        this.address = last;
    }

    public String imageUrl;
    public String title;
    public String first;
    public String last;
    public String email;
    public String birthday;
    public String password;
    public String phone;
    public String address;

    @Override
    public String toString() {
        return "Person{" +
                "imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", first='" + first + '\'' +
                ", last='" + last + '\'' +
                ", email='" + email + '\'' +
                ", dob='" + birthday + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

