package rodischeele.contactcard.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rodischeele.contactcard.R;

/**
 * Created by Rodi on 9-6-2016.
 */
public class PasswordFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        String myValue = bundle.getString("password");

        View v = inflater.inflate(R.layout.contact_card_fragment, container, false);

        TextView infoTag = (TextView) v.findViewById(R.id.info_tag);
        infoTag.setText("My password is");

        TextView tv = (TextView) v.findViewById(R.id.person_info);
        tv.setText(myValue);
        return v;
    }
}
