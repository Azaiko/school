package rodischeele.contactcard.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import rodischeele.contactcard.classes.Person;

/**
 * Created by Rodi on 8-6-2016.
 */
public class PersonDBHandler extends SQLiteOpenHelper {

    private static final String TAG = "PersonDBHandler";

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "person.db";
    private static final String DB_TABLE_NAME = "persons";

    // Tabel en kolom namen ...
    private static final String COLOMN_ID = "_id";  // primary key, auto increment
    private static final String COLOMN_FIRSTNAME = "firstName";
    private static final String COLOMN_LASTNAME = "lastName";
    private static final String COLOMN_IMAGEURL = "imageUrl";
    private static final String COLOMN_EMAIL = "email";
    private static final String COLOMN_ADDRESS = "address";
    private static final String COLOMN_PASSWORD = "password";
    private static final String COLOMN_TELEPHONE = "phone";
    private static final String COLOMN_BIRTHDAY = "birthday";


    // Default constructor
    public PersonDBHandler(Context context, String name,
                           SQLiteDatabase.CursorFactory factory,
                           int version) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    // Als de db niet bestaat wordt de db gemaakt. In de onCreate() de query
    // voor de aanmaak van de database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PERSON_TABLE = "CREATE TABLE " + DB_TABLE_NAME +
                "(" +
                COLOMN_ID + " INTEGER PRIMARY KEY," +
                COLOMN_FIRSTNAME + " TEXT," +
                COLOMN_LASTNAME + " TEXT," +
                COLOMN_IMAGEURL + " TEXT" +
                COLOMN_EMAIL + " TEXT" +
                COLOMN_ADDRESS + " TEXT" +
                COLOMN_PASSWORD + " TEXT" +
                COLOMN_TELEPHONE + " TEXT" +
                COLOMN_BIRTHDAY + " TEXT" +
                ")";
        db.execSQL(CREATE_PERSON_TABLE);
    }

    // Bij verandering van de db wordt onUpgrade aangeroepen.
    // Wat zou je hier kunnen doen? Weggooien en opnieuw aanmaken?
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        onCreate(db);
    }

    // CRUD functies hier ....

    public void addPerson(Person person)
    {
        ContentValues values = new ContentValues();
        values.put(COLOMN_FIRSTNAME, person.getFirst());
        values.put(COLOMN_LASTNAME, person.getLast());
        values.put(COLOMN_IMAGEURL, person.getImageUrl());
        values.put(COLOMN_EMAIL, person.getEmail());
        values.put(COLOMN_ADDRESS, person.getAddress());
        values.put(COLOMN_PASSWORD, person.getPassword());
        values.put(COLOMN_TELEPHONE, person.getPhone());
        values.put(COLOMN_BIRTHDAY, person.getBirthday());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DB_TABLE_NAME, null, values);
        db.close();
    }

    public void getPersonByFirstName(String firstName) {

        String query_a = "SELECT * FROM " + DB_TABLE_NAME + " WHERE " +
                COLOMN_FIRSTNAME + "=" + "\"" + firstName + "\"";

        String query_b = "SELECT * FROM " + DB_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query_b, null);

        cursor.moveToFirst();
        while(cursor.moveToNext() ) {
            Log.i(TAG, cursor.getString(cursor.getColumnIndex(COLOMN_FIRSTNAME)));
            Log.i(TAG, cursor.getString(cursor.getColumnIndex(COLOMN_LASTNAME)));
            Log.i(TAG, cursor.getString(cursor.getColumnIndex(COLOMN_IMAGEURL)));
            Log.i(TAG, "--------------------------------------------");
        }

        db.close();
    }

}

